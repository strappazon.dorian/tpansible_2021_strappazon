J'ai eu quelque soucis avec la partie MySQL:
La config fonctionnait bien jusqu'au moment où j'ai dû essayer avec des VM "propres" donc après la première provision et sans avoir lancé "ansible-playbook -i inventory play.yml".
Pour une raison obscure que je ne comprends toujours pas je n'ai plus le droit d'accèder à MySQL sur la machine web alors qu'avant j'avais bien réussi à créer la BDD et ajouter les utilisateurs depuis la liste "liste.yml".
J'ai essayé beaucoup de "solutions" en vain, du coup j'ai simplement commenté toute la partie sur MySQL.
Mise à part la partie sur MySQL tout le reste fonctionne bien.
